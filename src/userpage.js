import React, { useEffect, useState } from 'react';
import './userpage.css';
import { useParams, useNavigate} from 'react-router-dom';
import { fetchUsersDetails } from './networkuserpage';

export default function UserPage() {

    const params = useParams()
    const navigate = useNavigate()

    const [user, setUser] = useState({
        id: '',
        avatar: '',
        firstName: '',
        lastName: '',
        position: ''
    })

    useEffect(() => {
        console.log(params)
        fetchUsersDetails(params.id, setUser, navigate)
    }, [])

    useEffect(() => {

        console.log(user)
    }, [user])

  return (
        <div className='us'>
            {user ? (
                <div className='user-cont'>
                    <img src={user.avatar} alt='img' />
                    <p className='des'>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p>
                    <div className='name-cont'>
                        <h3>{user.firstName} {user.lastName}</h3>
                    </div>
                    <p className='pos'>{user.position}</p>
                </div>
            ) : (
                <div>No</div>
            )}
        </div>
    );

}