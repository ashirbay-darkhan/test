import React from 'react';
import './joinus.css';
import validate from './emailvalidator.js'
import { useSelector } from 'react-redux';
import { useActions } from './store/useActions';
import { subscribeUserFetch, unsubscribeUserFetch } from './neworkjoinus';

function JoinUs() {

  const user = localStorage.getItem('user')
  const [inputData, setInputData] = React.useState('')

  const pressedSubscribeState = useSelector(state => state.reducers.joinUsSubscribeReducer.pressedSubscribeValue)   
  const pressedUnsubscribedState = useSelector(state => state.reducers.joinUsSubscribeReducer.pressedUnsubscribeValue)

  const { pressedSubscribe, pressedUnsubscribe} = useActions()

  const subscribeUser = () => {
    console.log(inputData)
    const isValid = validate(inputData)
    subscribeUserFetch(isValid, inputData)
    pressedSubscribe()
    pressedUnsubscribe()
  }

  const unsubscribeUser = () => {
    unsubscribeUserFetch()
    pressedUnsubscribe()
    setInputData('')
    localStorage.removeItem('user')
  }

  React.useEffect(() => {
    console.log(pressedSubscribeState)
    if (user) {
        pressedUnsubscribe(false)
        pressedSubscribe(true)
    }
  },[])

  return (
    <section className='join-program-section'>
        <div className='join-program-section__layer'>
            <h2 className='join-program-section__layer--title'>  
                Join Our Program
            </h2>
            <h3 className='join-program-section__layer--subtitle'>
                Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </h3>
            {(user) ? (
                <button data-testid='unsub-but' id='email-button' className={`join-program-section__layer__email-form__button--subscribe  ${pressedUnsubscribedState ? `disable` : ``}`}
                onClick={unsubscribeUser} disabled={pressedUnsubscribedState}>
                  Unsubscribe
                </button>
            ) : (
            <form className='join-program-section__layer__email-form'>
                <input id='email' type='email' placeholder='Email' className='join-program-section__layer__email-form__email-input'
                onChange={ e => setInputData(e.target.value)}>
                </input>
                <button data-testid = 'sub-but' id={`email-button`} disabled={pressedSubscribeState} type='submit' onClick={subscribeUser} className={`join-program-section__layer__email-form__button--subscribe  ${pressedSubscribeState ? `disable` : ``}`}>
                    Subscribe
                </button>
            </form>
            )}
        </div>
    </section>
  );
}

export default JoinUs;