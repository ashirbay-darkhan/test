import { createSlice } from "@reduxjs/toolkit";

export const sectionSlice = createSlice({
    name: 'section',
    initialState: {
        value: false
    },
    reducers: {
        showSection: (state) => {
            state.value = true
        },
        hideSection: (state) => {
            state.value = false
        },
    }
}) 

export const { actions, reducer } = sectionSlice

