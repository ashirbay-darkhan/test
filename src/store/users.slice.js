import { createSlice } from "@reduxjs/toolkit";
import { getUserById } from "./users.actions";

export const usersSlice = createSlice({
    name: 'users',
    initialState: {
        isLoading: false,
        error: null,
        users: []
    },
    reducers: {
    },
    extraReducers: builder => {
        builder.addCase(getUserById.pending, state => {
            state.isLoading = true
        })
        .addCase(getUserById.fulfilled, (state, action) => {
            state.isLoading = false
            state.users = action.payload
        })
    }
}) 

export const { actions, reducer } = usersSlice

