import { bindActionCreators } from "@reduxjs/toolkit"
import { useMemo } from "react"
import { useDispatch } from "react-redux"
import { actions as sectionActions } from "./section.slice"
import { actions as JoinUsSubscribeActions} from "./joinus.sub.slice"
import  * as usersActions from "./users.actions"

const  rootActions = {
    ...sectionActions, ...JoinUsSubscribeActions, ...usersActions
}

export const useActions = () => {
    const dispatch = useDispatch()

    return useMemo(() => bindActionCreators(rootActions, dispatch), [dispatch])
}