import { createAsyncThunk } from "@reduxjs/toolkit"

const fetchUserbyId = async (userId) => {

  const response = await fetch(`http://localhost:3000/community/`);
  userId = await response.json();
  console.log(userId);
  return userId;
}


export const getUserById = createAsyncThunk('users/by-id', async(userId, thunkApi) => {
   try {
    const response = await fetchUserbyId(userId)
    return response
   } catch (error) {
        thunkApi.rejectWithValue(error)
    }
})