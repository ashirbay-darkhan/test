import { createSlice } from "@reduxjs/toolkit";

export const joinUsSubscribeSlice = createSlice({
    name: 'section',
    initialState: {
        pressedSubscribeValue: false,
        pressedUnsubscribeValue: false
    },
    reducers: {
        pressedSubscribe: (state) => {
            if (state.pressedSubscribeValue === true) {
                state.pressedSubscribeValue = false 
            } else {
                state.pressedSubscribeValue = true
            }
        },
        pressedUnsubscribe: (state) => {
            if (state.pressedUnsubscribeValue === false) {
                state.pressedUnsubscribeValue = true 
            } else {
                state.pressedUnsubscribeValue = false
            }
        },
    }
}) 

export const { actions, reducer } = joinUsSubscribeSlice