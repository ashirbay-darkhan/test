import { combineReducers, configureStore } from '@reduxjs/toolkit'
import {reducer as sectionReducer} from './section.slice'
import {reducer as joinUsSubscribeReducer} from './joinus.sub.slice'
import {reducer as usersReducer} from './users.slice'

const reducers = combineReducers({
    sectionReducer,
    joinUsSubscribeReducer,
    usersReducer
})

export default configureStore({
  reducer: {reducers},
  devTools: true,
})