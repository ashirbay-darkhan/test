import validate from "../emailvalidator";

describe('validate function', () => {
    test('returns true for valid email addresses', () => {
      expect(validate('example@gmail.com')).toBe(true);
      expect(validate('test@hotmail.com')).toBe(true);
    });
  
    test('returns false for invalid email addresses', () => {
      expect(validate('example@yahoo.com')).toBe(false);
      expect(validate('test@')).toBe(false);
      expect(validate(null)).toBe(false);
      expect(validate(undefined)).toBe(false);
    });
});