import { render, screen } from '@testing-library/react';
import { BrowserRouter, MemoryRouter } from 'react-router-dom';
import App from '../App';
import Comminity from "../community";

test('renders the NotFound component for any other route', () => {
  render(
    <MemoryRouter initialEntries={['/random']}>
      <App />
    </MemoryRouter>
  );

  const linkElement = screen.getByText(/Not Found/i);
  expect(linkElement).toBeInTheDocument();
});