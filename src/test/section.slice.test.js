import { actions, reducer } from '../store/section.slice';

describe('sectionSlice', () => {
  test('should set showSection', () => {
    const expected = { value: true };
    const result = reducer({ value: false }, actions.showSection());
    expect(result).toEqual(expected);
  });

  test('should set hideSection', () => {
    const expected = { value: false };
    const result = reducer({ value: true }, actions.hideSection());
    expect(result).toEqual(expected);
  });
});