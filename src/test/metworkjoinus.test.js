import { subscribeUserFetch, unsubscribeUserFetch } from "../neworkjoinus";

describe('subscribeUserFetch', () => {
  beforeEach(() => {
    jest.spyOn(window, 'fetch')
  })

  afterEach(() => {
    jest.restoreAllMocks()
  })

  test('should make a POST request to /subscribe endpoint with email in the body', () => {
    const inputData = 'john.doe@example.com'
    const isValid = true

    subscribeUserFetch(isValid, inputData)

    expect(window.fetch).toHaveBeenCalledWith('http://localhost:3000/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email: inputData })
    })
  })

  test('should handle a 200 response status and set local storage', async () => {
    const inputData = 'john.doe@example.com'
    const isValid = true

    jest.spyOn(window, 'alert').mockImplementation(() => {})

    jest.spyOn(window.localStorage.__proto__, 'setItem')

    window.fetch.mockResolvedValueOnce({
      status: 200
    })

    await subscribeUserFetch(isValid, inputData)

    expect(window.alert).toHaveBeenCalledWith('Email is successfully subscribed!')
    expect(window.localStorage.setItem).toHaveBeenCalledWith('user', inputData)
  })

  test('should handle a 400 response status and set local storage', async () => {
    const inputData = 'john.doe@example.com'
    const isValid = true

    jest.spyOn(window, 'alert').mockImplementation(() => {})

    jest.spyOn(window.localStorage.__proto__, 'setItem')

    window.fetch.mockResolvedValueOnce({
      status: 400
    })

    await subscribeUserFetch(isValid, inputData)

    expect(window.alert).toHaveBeenCalledWith('Wrong payload')
    expect(window.localStorage.setItem).toHaveBeenCalledWith('state', 'false')
  })

  test('should handle a 422 response status and set local storage', async () => {
    const inputData = 'john.doe@example.com'
    const isValid = true

    jest.spyOn(window, 'alert').mockImplementation(() => {})

    jest.spyOn(window.localStorage.__proto__, 'setItem')

    window.fetch.mockResolvedValueOnce({
      status: 422
    })

    await subscribeUserFetch(isValid, inputData)

    expect(window.alert).toHaveBeenCalledWith('Email is already in use')
    expect(window.localStorage.setItem).toHaveBeenCalledWith('state', 'false')
  })
})


describe('unsubscribeUserFetch', () => {
    beforeEach(() => {
      jest.spyOn(window, 'fetch')
    })
  
    afterEach(() => {
      jest.restoreAllMocks()
    })
  
    test('should make a POST request to /unsubscribe endpoint', () => {
      unsubscribeUserFetch()
  
      expect(window.fetch).toHaveBeenCalledWith('http://localhost:3000/unsubscribe', {
        method: 'POST'
      })
    })

    test('should handle a 200 response status and show an alert', async () => {
        jest.spyOn(window, 'alert').mockImplementation(() => {})
    
        window.fetch.mockResolvedValueOnce({
          status: 200
        })
    
        await unsubscribeUserFetch()
    
        expect(window.alert).toHaveBeenCalledWith('Email was unsubscribed!')
    })
})