import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import UserPage from '../userpage';

jest.mock('../networkuserpage', () => ({
  fetchUsersDetails: jest.fn().mockResolvedValue({
    id: '2f1b6bf3-f23c-47e4-88f2-e4ce89409376',
    avatar: 'https://example.com/avatar.png',
    firstName: 'John',
    lastName: 'Doe',
    position: 'Software Engineer',
  }),
}));

describe('UserPage', () => {
  test('should render user details when fetched successfully', async () => {
    render(
      <MemoryRouter initialEntries={['/community/2f1b6bf3-f23c-47e4-88f2-e4ce89409376']}>
        <Routes>
            <Route path="/community/:id" element={<UserPage />}>
            </Route>
        </Routes>
        
      </MemoryRouter>
    );

    const nameElement = await screen.findByText('Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.');

    expect(nameElement).toBeInTheDocument();
  });
});