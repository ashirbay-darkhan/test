
import { fetchUsersDetails } from '../networkuserpage';

describe('fetchUsersDetails', () => {
  it('should call the correct API endpoint and update the user state if successful', async () => {
    const user = 'johndoe';
    const setUser = jest.fn();
    const navigate = jest.fn();

    const mockResponse = { id: '123', name: 'John Doe' };
    jest.spyOn(global, 'fetch').mockResolvedValueOnce({
      status: 200,
      json: jest.fn().mockResolvedValueOnce(mockResponse),
    });

    const result = await fetchUsersDetails(user, setUser, navigate);

    expect(fetch).toHaveBeenCalledWith(`http://localhost:3000/community/${user}`);
    expect(setUser).toHaveBeenCalledWith(mockResponse);
    expect(result).toEqual(mockResponse);
    expect(navigate).not.toHaveBeenCalled();
  });

  it('should navigate to not-found page if user not found', async () => {
    const user = 'johndoe';
    const setUser = jest.fn();
    const navigate = jest.fn();

    jest.spyOn(global, 'fetch').mockResolvedValueOnce({
      status: 404,
    });

    await fetchUsersDetails(user, setUser, navigate);

    expect(fetch).toHaveBeenCalledWith(`http://localhost:3000/community/${user}`);
    expect(setUser).not.toHaveBeenCalled();
    expect(navigate).toHaveBeenCalledWith('/community/not-found');
  });
});