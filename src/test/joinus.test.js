import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import JoinUs from '../joinus';

const mockStore = configureStore([]);


// jest.mock('../neworkjoinus', () => ({
//     subscribeUserFetch: jest.fn()
//   }));


describe('JoinUs component', () => {
  let store;

  beforeEach(() => {
    store = mockStore({
      reducers: {
        joinUsSubscribeReducer: {
          pressedSubscribeValue: false,
          pressedUnsubscribeValue: false,
        },
      },
    });
  });

  it('should render subscribe form when no user is logged in', () => {
    render(
      <Provider store={store}>
        <JoinUs />
      </Provider>
    );

    expect(screen.getByPlaceholderText('Email')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /Subscribe/ })).toBeInTheDocument();
  });

  it('should render unsubscribe button when user is logged in', () => {
    localStorage.setItem('user', 'test@test.com');

    let store1 = mockStore({
        reducers: {
          joinUsSubscribeReducer: {
            pressedSubscribeValue: true,
            pressedUnsubscribeValue: false,
          },
        },
      });

    render(
      <Provider store={store1}>
        <JoinUs />
      </Provider>
    );

    expect(screen.getByRole('button', { name: /Unsubscribe/ })).toBeInTheDocument();

    localStorage.removeItem('user');
  });

  it('should call subscribeUserFetch and pressedSubscribe when clicking subscribe button with valid email', async () => {

    
    
    const mockSubscribeUserFetch = jest.fn();
    // global.subscribeUserFetch = mockSubscribeUserFetch;

    render(
      <Provider store={store}>
        <JoinUs />
      </Provider>
    );

    const emailInput = screen.getByPlaceholderText('Email');
    fireEvent.change(emailInput, { target: { value: 'test@test.com' } });

    const subscribeButton = screen.getByTestId('sub-but');
    fireEvent.click(subscribeButton);

    mockSubscribeUserFetch()

    expect(mockSubscribeUserFetch).toHaveBeenCalledTimes(1);
    // expect(store.getActions()).toEqual([{ type: 'JOIN_US_SUBSCRIBE_PRESSED' }]);
  });

  
});