import React from "react";
import { render, fireEvent, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import '@testing-library/jest-dom';
import Comminity from "../community";
import thunk from 'redux-thunk';


const mockStore = configureMockStore([thunk]);

describe('Community', () => {

    let usersStore;
    let hideStore;

    beforeEach(() => {
        usersStore = mockStore({
        reducers: {
          sectionReducer: {
            value: false,
          },
          usersReducer: {
            users: [],
            isLoading: false,
            error: null,
          },
        },
      });
    });

    it('should render the component', () => {
        render(
          <Provider store={usersStore}>
            <Comminity />
          </Provider>
        );
        expect(screen.getByText('Show section')).toBeInTheDocument();
    })

    it('should show  the section when the button is clicked', () => {

        render(
          <Provider store={usersStore}>
            <Comminity />
          </Provider>
        );

        const button = screen.getByRole('button');
        fireEvent.click(button);
        expect(button).toBeInTheDocument();
    });

    it('should hide the section when the button is clicked', () => {

        hideStore = mockStore({
            reducers: {
              sectionReducer: {
                value: true,
              },
              usersReducer: {
                users: [],
                isLoading: false,
                error: null,
              },
            },
          });

        render(
          <Provider store={hideStore}>
            <Comminity />
          </Provider>
        );

        const button = screen.getByRole('button');
        fireEvent.click(button);
        expect(button).toBeInTheDocument();
    });
    
})