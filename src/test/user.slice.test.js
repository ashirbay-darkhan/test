import { reducer } from '../store/users.slice'
import { getUserById } from "../store/users.actions"

describe('users slice', () => {
  it('should set isLoading to true when getUserById.pending is dispatched', () => {
    const initialState = { isLoading: false }
    const action = getUserById.pending()

    const newState = reducer(initialState, action)

    expect(newState.isLoading).toBe(true)
  })

  it('should set isLoading to false when getUserById.fulfilled is dispatched', () => {
    const initialState = { isLoading: true }
    const action = getUserById.fulfilled()

    const newState = reducer(initialState, action)

    expect(newState.isLoading).toBe(false)
  })

})