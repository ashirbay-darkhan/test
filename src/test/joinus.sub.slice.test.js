import { actions, reducer } from '../store/joinus.sub.slice'

describe('joinUsSubscribeSlice', () => {
  it('should toggle pressedSubscribeValue', () => {
    const initialState = {
      pressedSubscribeValue: false,
      pressedUnsubscribeValue: false
    }
    const nextState = reducer(initialState, actions.pressedSubscribe())
    expect(nextState.pressedSubscribeValue).toBe(true)

    const changedState = {
        pressedSubscribeValue: true,
        pressedUnsubscribeValue: false
    }

    const nextState2 = reducer(changedState, actions.pressedSubscribe())
    expect(nextState2.pressedSubscribeValue).toBe(false)
  })

  it('should toggle pressedUnsubscribeValue', () => {
    const initialState = {
      pressedSubscribeValue: false,
      pressedUnsubscribeValue: false
    }
    const nextState = reducer(initialState, actions.pressedUnsubscribe())
    expect(nextState.pressedUnsubscribeValue).toBe(true)

    const changedState = {
        pressedSubscribeValue: false,
        pressedUnsubscribeValue: true
    }

    const nextState2 = reducer(changedState, actions.pressedUnsubscribe())
    expect(nextState2.pressedUnsubscribeValue).toBe(false)
  })
})