const VALID_EMAIL_ENDINGS = ['gmail.com', 'hotmail.com'];

export default function validate(email) {

  if (typeof email === 'string' && email.length !== 0) {
    if (email.startsWith('@')) {
      return false;
    } else {
      const emailEnding = email.split('@')[1];
      return VALID_EMAIL_ENDINGS.includes(emailEnding)
    }
  } else {
    return false
  }
}