import React from 'react';
import './App.css';
import Comminity from './community';
import JoinUs from './joinus';
import { Routes, Route } from 'react-router-dom';
import UserPage from './userpage';
import NotFound from './NotFound';

function App() {

  return (
      <Routes>
        <Route path='/' element={<JoinUs/>} />
        <Route path='/community' element={<Comminity />}>
        </Route>
          <Route path={`/community/:id`} element={<UserPage />} />
        <Route path="/community/not-found" element={<NotFound />} />
        <Route path='*' element={<NotFound />} />
      </Routes>
  );
}

export default App;
