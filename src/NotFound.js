import React from 'react';
import './NotFound.css';
import { Link } from 'react-router-dom';

function NotFound() {
  return (
    <div className='back'>
        <div className='cont'>
            <h1 className='title'>Page Not Found</h1>
            <p className='des-nf'>Looks like you've followed a broken link or entered a URL. that doesn't <br/>
                exist on this site
            </p>
            <Link className='back-to' to={'/'}>&#8592; Back to our site</Link>
        </div>
    </div>
  );
}

export default NotFound;