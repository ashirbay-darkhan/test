
export const fetchUsersDetails = async (user, setUser, navigate) => {
    const response = await fetch(`http://localhost:3000/community/${user}`);
    if (response.status === 200) {
        const result = await response.json();
        setUser(result)
        return result
    } else {
        navigate("/community/not-found")
    }
}