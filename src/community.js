import React from 'react';
import './community.css';
import { useSelector } from 'react-redux';
import { useActions } from './store/useActions';
import { Link } from 'react-router-dom';

function Comminity() {

  const [name, setName] = React.useState('Show section')

  const visibleState = useSelector(state => state.reducers.sectionReducer.value)
  const {users, isLoading, error} = useSelector(state => state.reducers.usersReducer)

  const { showSection, hideSection } = useActions()

  const { getUserById } = useActions()

  const toggleCommunity = () => {

    if (visibleState === false) {
      showSection()
      setName('Show section')
    } else if (visibleState === true) {
      hideSection()
      setName('Hide section')
    }
  }

  React.useEffect(() => {
    getUserById()
    console.log('users :' + users.map(user => user.firstName))
  },[])

  return (
    <section className='big-com-section'>
      <div className='big-com-section__text-container'>
   
          <h2 className='big-com-section__text-container__title'>
          Big Community of People Like You
          </h2>
          <button data-testid="sec-but" type='button' className='toggle-button' onClick={toggleCommunity}>
          {name}
        </button>

        {visibleState === true && isLoading ? (
          <div>
            Loading...
          </div>
        ) : visibleState === true && error ? (
          <div>
            {error.message}
          </div>
        ) :  visibleState === true && users ? (
          <div className='b-sec'>
          <h3 className='big-com-section__text-container__subtitle'>
              We’re proud of our products, and we’re really excited when we get feedback from our users.
          </h3>
          <div className='big-com-section__card-container'>

            {users.map(user => 

              <div key={user.id} className='big-com-section__card-container__card'>
                <img className='big-com-section__card-container__card__img' src={user.avatar} alt='avatar' />
                <p className='big-com-section__card-container__card__des'>
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.
                </p>
                <Link className='user-link' to={`/community/${user.id}`
              } >
                  <p className='big-com-section__card-container__card__name'>
                    {user.firstName} {user.lastName}
                  </p>
                </Link>
                <p className='big-com-section__card-container__card__pos'>
                  {user.position}
                </p>
              </div>
              
              )}
            
          </div>
        </div>
        ) : (
          <div></div>
        )
      }
      </div>

    </section>
  );
}

export default Comminity;