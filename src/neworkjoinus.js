

const subscribeUserFetch = (isValid, inputData) => {
    if (isValid) {
        fetch('http://localhost:3000/subscribe', {
            method: 'POST',

            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({email: inputData})
        })
        .then(response => {

            switch (response.status) {
                case 200:
                    alert('Email is successfully subscribed!');
                    localStorage.setItem('user', inputData)
                    break
                case 400:
                    alert('Wrong payload');
                    localStorage.setItem('state', 'false')
                    break
                case 422:
                    alert('Email is already in use');
                    localStorage.setItem('state', 'false')
                    break
                default:
                    break;
            }
        })
        .catch(error => {
            alert(error.message);
        });   
    }
}

const unsubscribeUserFetch = () => {
    fetch('http://localhost:3000/unsubscribe', {
                    method: 'POST'
                })
                .then(response => {
                    alert('Email was unsubscribed!')
                    console.log(response)
                    
    
                })
                .catch(error => {
                    alert(error.message);
    });
}

export { subscribeUserFetch, unsubscribeUserFetch }
